# Date: 22-01-2020

# Updates
* I set up a Gitlab project
* Collated notes and research and uploaded them to my repository

# Discussion
* Mathematical formulation of problem uses the payoff function
* Next step is to try and formulate this in prism with 2 players

# To do
* Have a first model with 2 players
* Let players have different actions
* Start with a 1-shot game and then have a different number of turns