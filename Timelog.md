# Timelog

* Formal Modelling and Analysis of the Public Goods Game
* Pierre Ezard
* 2250393
* Dr. Gethin Norman

## Guidance

* This file contains the time log for your project. It will be submitted along with your final dissertation.
* **YOU MUST KEEP THIS UP TO DATE AND UNDER VERSION CONTROL.**
* This timelog should be filled out honestly, regularly (daily) and accurately. It is for *your* benefit.
* Follow the structure provided, grouping time by weeks.  Quantise time to the half hour.

## Semester 2

### Week 1

#### 17/01/2020
* *0.5 hours* Meeting with supervisor

### Week 2

#### 20/01/2020
* *1 hour* Setting up and configuration of Gitlab repository
* *4.5 hours* Reading about DTMC, MDP and going through examples from research papers
* *4.5 hours* Reading on game theory, public goods game, normal form definitions and notes gathering

#### 21/01/2020
* *4.5 hours* Reading research papers on game theory, and doing examples of Nash equilibria, minmax, maxmin theorem and examples
* *1 hours* Watching YouTube videos about the public goods game and the prisoner's dilemma
* *3 hours* Read and looked into mathematical formulation of stochastic games and function of the public goods game

#### 22/01/2020
* *0.5 hours* Meeting with supervisor

### Week 3

#### 27/01/2020
* *4.5 hours* Finishing up writing up of notes on game theory
* *1 hours* Watching YouTube tutorials on reaching nash equilibria, correlated equilibria
* *3 hours* Working through prism tutorials and model checking. 

#### 28/01/2020
* *6 hours* Worked through examples of case studies and more tutorials and made my own implementations
* *4 hours* Started different models for a 2 player public good game

#### 29/01/2020
* *1 hours* Meeting with supervisor

### Week 4

#### 04/02/2020
* *9 hours* Finishing up working model, running it with 2 players, 2 choices to export strategy.
* *2 hours* Installed and ran graphviz to visualise and generate strategy figures.

#### 05/02/2020
* *0.5 hours* Meeting with supervisor

### Week 5

### 10/02/2020
* *4 hours* Made similar models as with 2 choices but with 4 and 8 possible actions for players and tested them. 
* *8 hours* Wrote bash scripts to run and export player strategies to different directories. Tested the models on f-values from 0.9 to 2.0 with different numbers of turns. 

#### 11/02/2020
* *8 hours* Modified bash scripts to run and ran tests on C2, C4, C8 for different number of turns and explored their interesting f-values. I also added a C11 model. 
* *4 hours* Went into interesting regions of f values and went through the results to record them. 
* *1 hour* Made payoffs table for different models

#### 12/02/2020
* *0.5 hours* Meeting with Gethin

#### 14/02/2020
* *3.5 hours* Implemented a model with profit and dynamic rewards 
* *1.5 hours* Updated this month's timelog and meeting minutes

### Week 6

#### 17/02/2020
* *10 hours* Corrected dpgg model with Gethin's feedback, wrote bash scripts to run experiments. 

#### 18/02/2020
* *10 hours* Wrote more bash scripts to organise the results and analysed the results for number of turns 1,2,3,5 for 3 different models. 

#### 19/02/2020
* *0.5 hours* Meeting with Gethin

#### 21/02/2020
* *0.5 hours* Update timelog and this week's meeting minutes
* *0.5 hours* Created an overleaf dissertation report based on the template

### Week 7

#### 24/02/2020
* *10.5 hours* Wrote scripts to get the experiments running correctly and aggregate them. Ran scripts for all pgg models and most of the dpgg models. Corrected some of the model renaming issues and contributions. 

#### 25/02/2020
* *11.5 hours* Wrote bash scripts to clean all the corrupt (empty) files, re-sort according to f-values to produce good graphs. Used matplotlib to auto-generate graphs for all models and bash scripts to parametrise both models and number of turns. 

#### 21/02/2020
* *0.5 hours* Meeting with Gethin

#### 01/03/2020
* *0.5 hours* Updated timelog and weekly meeting notes
* *5.5 hours* Ran all experiments again, checking the interesting ranges for f, generating graphs for all models in pgg and dpgg
* *0.5 hours* Re-changed the contributions for dpgg c4 and c8 as they were counter-intuitive
* *1 hours* Changed bash-scripts variables