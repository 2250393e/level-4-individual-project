import os
import sys
import glob
import re
import unittest

# arugments: [model, number_turns]
c_model = sys.argv[1]
n_turns = "k" + sys.argv[2]

cwd = os.getcwd()
path = cwd+"/"+c_model+"/p3/"+"coa/"
dir = os.listdir(path)

def extract_coas(model, number_turns):
    cwd = os.getcwd()
    path = cwd+"/"+model+"/p3/"+"coa/"
    dir = os.listdir(path)
    files = []

    for filename in dir:
        if (number_turns in filename and "ALL" in filename):
            files.append(filename)

    for filename in files:
        filepath = path + filename
        file = open(filepath, "r")
        content = file.read()
        matched_lines = [line for line in content.split('\n') if "Result for coalition" in line]
        coa1 = re.findall("\d+\.\d+", matched_lines[0])[0]
        coa2 = re.findall("\d+\.\d+", matched_lines[1])[0]
        coa3 = re.findall("\d+\.\d+", matched_lines[2])[0]
        output = "Coalition results: (" +coa1+","+coa2+","+coa3+")"
        factor = re.sub(r'.*f', 'f', filepath)
        factor = factor.replace("f", "")
        factor = factor.replace(".txt", "")
        output_filename = cwd+"/"+model+"/p3/"+"coa/"+str(number_turns)+"_f"+factor+".txt"
        output_file = open(output_filename, "w")
        n = output_file.write(output)
        output_file.close()
        print(output_filename)
        file.close()

extract_coas(c_model, n_turns)