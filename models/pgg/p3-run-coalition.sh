args=("$@")
actions=${args[0]}
players="p3"
f1=${args[1]}
f2=${args[2]}
step=${args[3]}

for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$actions/p3/coa/ALL_k1_f$f.txt

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism pgg_p3_$actions.prism pgg_p3.props -const k=1 -const f=$f > $FILE
        echo "saved: "$FILE
    else 
        echo "already exists: "$FILE
    fi
done