csg

player player1 contributor1 endplayer
player player2 contributor2 endplayer

// define our constants
const int tokens1=5; // initial number of tokens for player 1
const int tokens2=5; // initial number of tokens for player 2
const double f; //enhancement factor (can specify as a parameter)
const int k; //number of rounds

module contributor1

    c1: [0..tokens1] init 0; // need to give this a range

    [contribute1_0] s=0 & rounds<k -> (c1'=0); // keep all tokens
    [contribute1_1] s=0 & rounds<k -> (c1'=1); // contribute 1
    [contribute1_2] s=0 & rounds<k -> (c1'=2); // contribute 2
    [contribute1_3] s=0 & rounds<k -> (c1'=3); // contribute 3
    [contribute1_4] s=0 & rounds<k -> (c1'=4); // contribute 4
    [contribute1_5] s=0 & rounds<k -> (c1'=5); // contribute 5
    
    [update1] s=1 -> (c1'=0); // this reduces the state space
    
endmodule

// create a player2
module contributor2 = contributor1 [ 
	c1=c2,
    c2=c1,
	tokens1=tokens2, 
	contribute1_0=contribute2_0,
	contribute1_1=contribute2_1,
    contribute1_2=contribute2_2,
    contribute1_3=contribute2_3,
    contribute1_4=contribute2_4,
    contribute1_5=contribute2_5,
	update1=update2 ] 
endmodule 

// rewards
// sum of contributions shared 
rewards "utility1"
    s=1 : (tokens1 - c1) + floor(f*(c1+c2)/2);
endrewards

rewards "utility2"
    s=1 : (tokens2 - c2) + floor(f*(c1+c2)/2);
endrewards

// module to keep track of the number of rounds
// only update at the end of the round

module rounds // module to count the rounds
	rounds : [0..k+1];
	
	[] s=1 & rounds<=k -> (rounds'=rounds+1);
	[] s=1 & rounds=k+1 -> true;

endmodule

// module to have players perform actions concurrently at each step
module scheduler
    s : [0..1] init 0; // this should start from 0

    [] s=0 & rounds<=k -> (s'=1);
    [] s=1 & rounds<=k -> (s'=0);

endmodule