PRISM-games
===========

Version: 3.0.beta (based on PRISM 4.5.dev)
Date: Thu Apr 02 22:00:22 CEST 2020
Hostname: Pierres-MBP.fritz.box
Memory limits: cudd=1g, java(heap)=1g
Command line: prism-games pgg_p3_a4.prism pgg_p3.props -const k=1 -const f=1.65

Parsing model file "pgg_p3_a4.prism"...

Type:        CSG
Modules:     contributor1 contributor2 contributor3 rounds scheduler 
Variables:   c1 c2 c3 rounds s 

Switching to explicit engine, which supports CSGs...

Parsing properties file "pgg_p3.props"...

4 properties:
(1) <<player1>>R{"utility1"}max=? [ F rounds=k ]
(2) <<player2>>R{"utility2"}max=? [ F rounds=k ]
(3) <<player3>>R{"utility3"}max=? [ F rounds=k ]
(4) <<player1:player2:player3>>max=? (R{"utility1"}[F s=1&rounds=k] + R{"utility2"}[F s=1&rounds=k] + R{"utility3"}[F s=1&rounds=k])

---------------------------------------------------------------------

Model checking: <<player1>>R{"utility1"}max=? [ F rounds=k ]
Model constants: f=1.65,k=1

Building model...
Model constants: f=1.65,k=1

Computing reachable states... 68 states
Reachable states exploration and model construction done in 0.092 secs.
Sorting reachable states list...

Time for model construction: 0.114 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        CSG
States:      68 (1 initial)
Transitions: 131
Choices:     131
Max/avg:     64/1.93
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.22,1.04)
Max/avg (actions): (4,16)/(1.04,1.22)
target=2, inf=1, rest=65
Computing the upper bound where 0.06 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.167 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.295 seconds.
Precomputation took 0.082 seconds.

Value in the initial state: 2.9999999999999996

Time for model checking: 0.308 seconds.

Result: 2.9999999999999996 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player2>>R{"utility2"}max=? [ F rounds=k ]
Model constants: f=1.65,k=1
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.22,1.04)
Max/avg (actions): (4,16)/(1.04,1.22)
target=2, inf=1, rest=65
Computing the upper bound where 0.06 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.014 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.04 seconds.
Precomputation took 0.014 seconds.

Value in the initial state: 2.9999999999999996

Time for model checking: 0.041 seconds.

Result: 2.9999999999999996 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player3>>R{"utility3"}max=? [ F rounds=k ]
Model constants: f=1.65,k=1
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.22,1.04)
Max/avg (actions): (4,16)/(1.04,1.22)
target=2, inf=1, rest=65
Computing the upper bound where 0.06 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.005 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.076 seconds.
Precomputation took 0.051 seconds.

Value in the initial state: 2.9999999999999996

Time for model checking: 0.077 seconds.

Result: 2.9999999999999996 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player1:player2:player3>>max=? (R{"utility1"}[F s=1&rounds=k] + R{"utility2"}[F s=1&rounds=k] + R{"utility3"}[F s=1&rounds=k])
Model constants: f=1.65,k=1

Building extended model...

States:      68 (1 initial)
Transitions: 131
Choices:     131
Max/avg:     64/1.93

Max/avg (actions): (4,4,4)/(1.04,1.04,1.04)

Warning: Cannot use labelled polytoples on multi-coalitional properties. Changing to support enumeration.
0: (0.0,0.0,0.0)
0--1--2--3--4--5--6--7--8--9--10--11--12--13--14--15--16--17--18--19--20--21--22--23--24--25--26--27--28--29--30--31--32--33--34--35--36--37--38--39--40--41--42--43--44--45--46--47--48--49--50--51--52--53--54--55--56--57--58--59--60--61--62--63--1: (4.0,3.0,3.0)
0--1--2--3--4--5--6--7--8--9--10--11--12--13--14--15--16--17--18--19--20--21--22--23--24--25--26--27--28--29--30--31--32--33--34--35--36--37--38--39--40--41--42--43--44--45--46--47--48--49--50--51--52--53--54--55--56--57--58--59--60--61--62--63--2: (4.0,3.0,3.0)

Result for coalition player1: 4.0 (value in the intial state).
Result for coalition player2: 3.0 (value in the intial state).
Result for coalition player3: 3.0 (value in the intial state).

Value in the initial state: 10.0

Time for model checking: 0.641 seconds.

Result: 10.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 2 warnings during computation.

