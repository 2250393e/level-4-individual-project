PRISM-games
===========

Version: 3.0.beta (based on PRISM 4.5.dev)
Date: Thu Apr 02 22:00:41 CEST 2020
Hostname: Pierres-MBP.fritz.box
Memory limits: cudd=1g, java(heap)=1g
Command line: prism-games pgg_p3_a4.prism pgg_p3.props -const k=1 -const f=2.05

Parsing model file "pgg_p3_a4.prism"...

Type:        CSG
Modules:     contributor1 contributor2 contributor3 rounds scheduler 
Variables:   c1 c2 c3 rounds s 

Switching to explicit engine, which supports CSGs...

Parsing properties file "pgg_p3.props"...

4 properties:
(1) <<player1>>R{"utility1"}max=? [ F rounds=k ]
(2) <<player2>>R{"utility2"}max=? [ F rounds=k ]
(3) <<player3>>R{"utility3"}max=? [ F rounds=k ]
(4) <<player1:player2:player3>>max=? (R{"utility1"}[F s=1&rounds=k] + R{"utility2"}[F s=1&rounds=k] + R{"utility3"}[F s=1&rounds=k])

---------------------------------------------------------------------

Model checking: <<player1>>R{"utility1"}max=? [ F rounds=k ]
Model constants: f=2.05,k=1

Building model...
Model constants: f=2.05,k=1

Computing reachable states... 68 states
Reachable states exploration and model construction done in 0.036 secs.
Sorting reachable states list...

Time for model construction: 0.049 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        CSG
States:      68 (1 initial)
Transitions: 131
Choices:     131
Max/avg:     64/1.93
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.22,1.04)
Max/avg (actions): (4,16)/(1.04,1.22)
target=2, inf=1, rest=65
Computing the upper bound where 0.07 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.022 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.06 seconds.
Precomputation took 0.015 seconds.

Value in the initial state: 3.0

Time for model checking: 0.065 seconds.

Result: 3.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player2>>R{"utility2"}max=? [ F rounds=k ]
Model constants: f=2.05,k=1
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.22,1.04)
Max/avg (actions): (4,16)/(1.04,1.22)
target=2, inf=1, rest=65
Computing the upper bound where 0.07 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.007 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.02 seconds.
Precomputation took 0.006 seconds.

Value in the initial state: 3.0

Time for model checking: 0.02 seconds.

Result: 3.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player3>>R{"utility3"}max=? [ F rounds=k ]
Model constants: f=2.05,k=1
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.22,1.04)
Max/avg (actions): (4,16)/(1.04,1.22)
target=2, inf=1, rest=65
Computing the upper bound where 0.07 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.004 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.016 seconds.
Precomputation took 0.005 seconds.

Value in the initial state: 3.0

Time for model checking: 0.017 seconds.

Result: 3.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player1:player2:player3>>max=? (R{"utility1"}[F s=1&rounds=k] + R{"utility2"}[F s=1&rounds=k] + R{"utility3"}[F s=1&rounds=k])
Model constants: f=2.05,k=1

Building extended model...

States:      68 (1 initial)
Transitions: 131
Choices:     131
Max/avg:     64/1.93

Max/avg (actions): (4,4,4)/(1.04,1.04,1.04)

Warning: Cannot use labelled polytoples on multi-coalitional properties. Changing to support enumeration.
0: (0.0,0.0,0.0)
0--1--2--3--4--5--6--7--8--9--10--11--12--13--14--15--16--17--18--19--20--21--22--23--24--25--26--27--28--29--30--31--32--33--34--35--36--37--38--39--40--41--42--43--44--45--46--47--48--49--50--51--52--53--54--55--56--57--58--59--60--61--62--63--64--65--66--67--68--69--70--71--72--73--74--75--76--77--78--79--80--81--82--83--84--85--86--87--88--89--90--91--92--93--94--95--96--97--98--99--100--101--102--103--104--105--106--107--108--109--110--111--112--113--114--115--116--117--118--119--120--121--122--123--124--125--126--127--128--129--130--131--132--133--134--135--136--137--138--139--140--141--142--143--144--145--146--147--148--149--150--151--152--153--154--155--156--157--158--159--160--161--162--163--164--165--166--167--168--169--170--171--172--173--174--175--176--177--178--179--180--181--182--183--184--185--186--187--188--189--190--191--192--193--194--195--196--197--198--199--200--201--202--203--204--205--206--207--208--209--210--211--212--213--214--215--216--217--218--219--220--221--222--223--224--225--226--227--228--229--230--231--232--233--234--235--236--237--238--239--240--241--242--243--244--245--246--247--248--249--250--251--252--253--254--255--256--257--258--259--260--261--262--263--264--265--266--267--268--269--270--271--272--273--274--275--276--277--278--279--280--281--282--283--284--285--286--287--288--289--290--291--292--293--294--295--296--297--298--299--300--301--302--303--304--305--306--307--308--309--310--311--312--313--314--315--316--317--318--319--320--321--322--323--324--325--326--327--328--329--330--331--332--333--334--335--336--337--338--339--340--341--342--343--344--345--346--347--348--349--350--351--352--353--354--355--356--357--358--359--360--361--362--363--364--365--366--367--368--369--370--371--372--373--374--375--376--377--378--379--380--381--382--383--384--385--386--387--388--389--390--391--392--393--394--395--396--397--398--399--400--401--402--403--404--405--406--407--408--409--410--411--412--413--414--415--416--417--418--419--420--421--422--423--424--425--426--427--428--429--430--431--432--433--434--435--436--437--438--439--440--441--442--443--444--445--446--447--1: (5.0,5.0,5.0)
0--1--2--3--4--5--6--7--8--9--10--11--12--13--14--15--16--17--18--19--20--21--22--23--24--25--26--27--28--29--30--31--32--33--34--35--36--37--38--39--40--41--42--43--44--45--46--47--48--49--50--51--52--53--54--55--56--57--58--59--60--61--62--63--64--65--66--67--68--69--70--71--72--73--74--75--76--77--78--79--80--81--82--83--84--85--86--87--88--89--90--91--92--93--94--95--96--97--98--99--100--101--102--103--104--105--106--107--108--109--110--111--112--113--114--115--116--117--118--119--120--121--122--123--124--125--126--127--128--129--130--131--132--133--134--135--136--137--138--139--140--141--142--143--144--145--146--147--148--149--150--151--152--153--154--155--156--157--158--159--160--161--162--163--164--165--166--167--168--169--170--171--172--173--174--175--176--177--178--179--180--181--182--183--184--185--186--187--188--189--190--191--192--193--194--195--196--197--198--199--200--201--202--203--204--205--206--207--208--209--210--211--212--213--214--215--216--217--218--219--220--221--222--223--224--225--226--227--228--229--230--231--232--233--234--235--236--237--238--239--240--241--242--243--244--245--246--247--248--249--250--251--252--253--254--255--256--257--258--259--260--261--262--263--264--265--266--267--268--269--270--271--272--273--274--275--276--277--278--279--280--281--282--283--284--285--286--287--288--289--290--291--292--293--294--295--296--297--298--299--300--301--302--303--304--305--306--307--308--309--310--311--312--313--314--315--316--317--318--319--320--321--322--323--324--325--326--327--328--329--330--331--332--333--334--335--336--337--338--339--340--341--342--343--344--345--346--347--348--349--350--351--352--353--354--355--356--357--358--359--360--361--362--363--364--365--366--367--368--369--370--371--372--373--374--375--376--377--378--379--380--381--382--383--384--385--386--387--388--389--390--391--392--393--394--395--396--397--398--399--400--401--402--403--404--405--406--407--408--409--410--411--412--413--414--415--416--417--418--419--420--421--422--423--424--425--426--427--428--429--430--431--432--433--434--435--436--437--438--439--440--441--442--443--444--445--446--447--2: (5.0,5.0,5.0)

Result for coalition player1: 5.0 (value in the intial state).
Result for coalition player2: 5.0 (value in the intial state).
Result for coalition player3: 5.0 (value in the intial state).

Value in the initial state: 15.0

Time for model checking: 3.888 seconds.

Result: 15.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 2 warnings during computation.

