import os
import sys

p = sys.argv[2]
c_model = sys.argv[1]
cwd = os.getcwd()
path = cwd+"/"+c_model+"/"+p+"/"+"coa/"
dir = os.listdir(path)

files = []

for filename in dir:
    files.append(filename)

for filename in files:
    filepath = path + filename
    file = open(filepath, "r")
    line = file.read()
    if "Coa" in line or "Result for coalition player3:" in line:
        print("Fine: " + filename)
    else:
        print("corrupt file:" + filename)
        os.remove(filepath)
        print("File removed")
    file.close()