#f : enhancement factor = [0.9 -> 2.0 step 0.1]

args=("$@")
actions=${args[0]}
players="p3"
f1=${args[1]}
f2=${args[2]}
step=${args[3]}

for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$actions/p3/pdf/k1_f$f.pdf

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism pgg_p3_$actions.prism pgg_p3.props -const k=1 -const f=$f -exportstrat $actions/p3/dot/k1_f$f.dot
        dot -Tpdf $actions/p3/dot/k1_f$f.dot -o $actions/p3/pdf/k1_f$f.pdf
        echo "saved: "$FILE
    else 
        echo "already exists: "$FILE
    fi
done