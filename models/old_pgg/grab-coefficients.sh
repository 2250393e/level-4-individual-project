args=("$@")
model=${args[0]}
f1=${args[1]}
f2=${args[2]}
step=${args[3]}

# 1-turn
for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/c8/coa/k1_f$f.txt

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism pgg_p2_$model.prism pgg_p2.props -const k=1 -const f=$f | grep Coa > $model/coa/k1_f$f.txt
        echo Saving coa with k=1, f=$f
    else 
        echo "$model/coa/k1_f$f.txt already exist"
    fi
done

# 2-turn
for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/c8/coa/k2_f$f.txt

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism pgg_p2_$model.prism pgg_p2.props -const k=2 -const f=$f | grep Coa > $model/coa/k2_f$f.txt
        echo Saving coa with k=2, f=$f
    else 
        echo "$model/coa/k2_f$f.txt already exist"
    fi
done