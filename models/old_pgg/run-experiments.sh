#k : number of turns = [1,2,3,5,10,20]
#f : enhancement factor = [0.9 -> 2.0 step 0.1]

args=("$@")
model=${args[0]}
f1=${args[1]}
f2=${args[2]}
step=${args[3]}

# 1-turn
for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$model/pdf/k1_f$f.pdf

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism pgg_p2_$model.prism pgg_p2.props -const k=1 -const f=$f -exportstrat $model/dot/k1_f$f.dot
        dot -Tpdf $model/dot/k1_f$f.dot -o $model/pdf/k1_f$f.pdf
        echo "save $model/pdf/k1_f$f.pdf"
    else 
        echo "$model/pdf/k1_f$f.pdf already exist"
    fi

done

# 2-turn
for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$model/pdf/k2_f$f.pdf

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism pgg_p2_$model.prism pgg_p2.props -const k=2 -const f=$f -exportstrat $model/dot/k2_f$f.dot
        dot -Tpdf $model/dot/k2_f$f.dot -o $model/pdf/k2_f$f.pdf
        echo "save $model/pdf/k2_f$f.pdf"
    else 
        echo "$model/pdf/k2_f$f.pdf already exist"
    fi

done