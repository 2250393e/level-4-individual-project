cd pgg

python clean-empty-coa.py c2
python clean-empty-coa.py c4
python clean-empty-coa.py c8
python clean-empty-coa.py c11

./run-experiments.sh c2
./run-experiments.sh c4
./run-experiments.sh c8
./run-experiments.sh c11

./grab-coefficients.sh c2
./grab-coefficients.sh c4
./grab-coefficients.sh c8
./grab-coefficients.sh c11

python generate-graphs.py c2 1
python generate-graphs.py c2 2
python generate-graphs.py c2 3
python generate-graphs.py c4 1
python generate-graphs.py c4 2
python generate-graphs.py c4 3
python generate-graphs.py c8 1
python generate-graphs.py c8 2
python generate-graphs.py c8 3
python generate-graphs.py c11 1
python generate-graphs.py c11 2
python generate-graphs.py c11 3

cd ..
cd dpgg

python clean-empty-coa.py c2
python clean-empty-coa.py c4
python clean-empty-coa.py c8

./run-experiments.sh c2
./run-experiments.sh c4
./run-experiments.sh c8

./grab-coefficients.sh c2
./grab-coefficients.sh c4
./grab-coefficients.sh c8

python generate-graphs.py c2 1
python generate-graphs.py c2 2
python generate-graphs.py c2 3
python generate-graphs.py c4 1
python generate-graphs.py c4 2
python generate-graphs.py c4 3
python generate-graphs.py c8 1
python generate-graphs.py c8 2
python generate-graphs.py c8 3