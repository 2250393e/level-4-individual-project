import os
import sys

c_model = sys.argv[1]
cwd = os.getcwd()
path = cwd+"/"+c_model+"/"+"coa/"
dir = os.listdir(path)

files = []

for filename in dir:
    files.append(filename)

for filename in files:
    filepath = path + filename
    file = open(filepath, "r")
    line = file.read()
    if "Coa" in line:
        print("Fine: " + filename)
    else:
        print("corrupt file:" + filename)
        os.remove(filepath)
        print("File removed")
    file.close()