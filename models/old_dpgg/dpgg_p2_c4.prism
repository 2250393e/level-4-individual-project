csg

player player1 contributor1 endplayer
player player2 contributor2 endplayer

// define our constants
const int tokens1=10; // initial number of tokens for player 1
const int tokens2=10; // initial number of tokens for player 2
const double f; //enhancement factor (can specify as a parameter)
const int k; //number of rounds

module contributor1

    c1: [0..1000] init 0;
    p1: [-1000..1000] init tokens1; // variable to keep track of profit

    [contribute1_0] s=0 & rounds<k -> (c1'=0); // keep all tokens and don't contribute
    [contribute1_1] s=0 & rounds<k -> (c1'=floor(p1*(1/3))); //contribute all tokens
    [contribute1_2] s=0 & rounds<k -> (c1'=floor(p1*(2/3)));
    [contribute1_3] s=0 & rounds<k -> (c1'=p1);
    
    // contribution can use initial tokens and any profit realised in previous rounds
    // p1 can also be negative
    
    [update1] s=1 & rounds<k -> (c1'=0) & (p1'= p1 -c1 + floor(f*(c1+c2)/2)); // this reduces the state space
    // profit is updated as the previous profit - contribution of the round + the shared public pool of the round
    
endmodule

// create a player2
module contributor2 = contributor1 [ 
	c1=c2,
    c2=c1,
    p1=p2,
	tokens1=tokens2, 
	contribute1_0=contribute2_0,
	contribute1_1=contribute2_1,
    contribute1_2=contribute2_2,
    contribute1_3=contribute2_3,
	update1=update2 ] 
endmodule 

// rewards
// sum of contributions shared 
rewards "utility1"
    s=0 & rounds=k : (p1);
endrewards

rewards "utility2"
    s=0 & rounds=k : (p2);
endrewards

//module to keep track of the number of rounds
// only update at the end of the round

module rounds // module to count the rounds
	rounds : [0..k+1];
	
	[] s=1 & rounds<=k -> (rounds'=rounds+1);
	[] s=1 & rounds=k+1 -> true;

endmodule

//module to have players perform actions concurrently at each step
module scheduler
    s : [0..1] init 0; // this should start from 0

    [] s=0 & rounds<=k -> (s'=1);
    [] s=1 & rounds<=k -> (s'=0);

endmodule