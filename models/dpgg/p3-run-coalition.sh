args=("$@")
actions=${args[0]}
players="p3"
f1=${args[1]}
f2=${args[2]}
step=${args[3]}

for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$actions/p3/coa/ALL_k2_f$f.txt

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism dpgg_p3_$actions.prism dpgg_p3.props -const k=2 -const f=$f > $FILE
        echo "saved: "$FILE
    else 
        echo "already exists: "$FILE
    fi
done

for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$actions/p3/coa/ALL_k3_f$f.txt

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism dpgg_p3_$actions.prism dpgg_p3.props -const k=3 -const f=$f > $FILE
        echo "saved: "$FILE
    else 
        echo "already exists: "$FILE
    fi
done