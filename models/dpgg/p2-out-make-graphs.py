#!/usr/bin/python

import os
import sys
import glob
import re
import unittest

# arugments: [model, number_turns]
c_model = sys.argv[1]
n_turns = "k" + sys.argv[2]

cwd = os.getcwd()
path = cwd+"/"+c_model+"/p2/"+"coa/"
dir = os.listdir(path)

files = []

def parse_results(model, number_turns):
    cwd = os.getcwd()
    path = cwd+"/"+model+"/p2/"+"coa/"
    dir = os.listdir(path)

    factors = []
    p1_results = []
    p2_results = []

    for filename in dir:
        if (number_turns in filename):
            files.append(filename)

    for filename in files:
        filepath = path + filename
        file = open(filepath, "r")
        line = file.read()
        file.close()
        newLine = ''.join((ch if ch in '0123456789.' else ' ') for ch in line)
        listOfNumbers = [float(i) for i in newLine.split()]
        
        factor = re.sub(r'.*f', 'f', filepath)
        factor = factor.replace("f", "")
        factor = factor.replace(".txt", "")
        result_p1 = None
        result_p2 = None

        if (len(listOfNumbers) == 2):
            result_p1 = listOfNumbers[0]
            result_p2 = listOfNumbers[1]

        if (factor != None and result_p1 != None and result_p2 != None):
            if ( (float(factor)*10)%1 == 0 or (float(factor)*10)%1 == 0.5):
                factors.append(factor)
                p1_results.append(result_p1)
                p2_results.append(result_p2)

    if (not (len(factors) == len(p1_results) == len(p2_results))):
        print("Parsing Error: results for factors, p1 and p2 are not of same length")

    return factors, p1_results, p2_results

import itertools

f_NS, p1_NS, p2_NS = parse_results(c_model, n_turns)

if (not(len(f_NS) >= 1)):
    print("Error: No data available to produce the graph")
    sys.exit()

list_1 = sorted(itertools.izip(*[f_NS, p1_NS]))
list_2 = sorted(itertools.izip(*[f_NS, p2_NS]))

f, p1 = list(itertools.izip(*list_1))
f, p2 = list(itertools.izip(*list_2))


for i in range(len(f)):
    print(f[i], p1[i], p2[i])


import matplotlib.pyplot as plt

fig, ax = plt.subplots()
plt.plot(f, p1, '-o', label="p1")
plt.plot(f, p2, '-o', label="p2")
plt.title("Player payoffs (model: "+c_model + " , turns: " + sys.argv[2] +")")
plt.xlabel("Enhancement Factor (f)")
plt.ylabel("Player end-payoff")
ax.set_ylim(ymin=0)
ax.set_ylim(ymax=max(max(p1), max(p2))*1.3)
ax.legend()

graph_path = os.getcwd() + "/" + c_model + "/p2/graphs/" + c_model + "_" + n_turns + ".png"
plt.savefig(graph_path)