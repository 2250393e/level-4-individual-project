#f : enhancement factor = [0.9 -> 2.0 step 0.1]

args=("$@")
actions=${args[0]}
players="p2"
f1=${args[1]}
f2=${args[2]}
step=${args[3]}

: <<'END'

for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$actions/p2/pdf/k2_f$f.pdf

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism dpgg_p2_$actions.prism dpgg_p2.props -const k=2 -const f=$f -exportstrat $actions/p2/dot/k2_f$f.dot
        dot -Tpdf $actions/p2/dot/k2_f$f.dot -o $actions/p2/pdf/k2_f$f.pdf
        echo "saved: "$FILE
    else 
        echo "already exists: "$FILE
    fi
done



for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$actions/p2/pdf/k3_f$f.pdf

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism dpgg_p2_$actions.prism dpgg_p2.props -const k=3 -const f=$f -exportstrat $actions/p2/dot/k3_f$f.dot
        dot -Tpdf $actions/p2/dot/k3_f$f.dot -o $actions/p2/pdf/k3_f$f.pdf
        echo "saved: "$FILE
    else 
        echo "already exists: "$FILE
    fi
done

END

for f in $(seq $f1 $step $f2)
do
    FILE=$PWD/$actions/p2/pdf/k4_f$f.pdf

    if ! [ -f "$FILE" ]; then
        ../../prism/bin/prism dpgg_p2_$actions.prism dpgg_p2.props -const k=4 -const f=$f -exportstrat $actions/p2/dot/k4_f$f.dot
        dot -Tpdf $actions/p2/dot/k4_f$f.dot -o $actions/p2/pdf/k4_f$f.pdf
        echo "saved: "$FILE
    else 
        echo "already exists: "$FILE
    fi
done