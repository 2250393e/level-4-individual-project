PRISM-games
===========

Version: 3.0.beta (based on PRISM 4.5.dev)
Date: Sat Apr 04 19:54:06 CEST 2020
Hostname: Pierres-MBP.fritz.box
Memory limits: cudd=1g, java(heap)=1g
Command line: prism-games dpgg_p3_a4.prism dpgg_p3.props -const k=2 -const f=1.6

Parsing model file "dpgg_p3_a4.prism"...

Type:        CSG
Modules:     contributor1 contributor2 contributor3 rounds scheduler 
Variables:   c1 p1 c2 p2 c3 p3 rounds s 

Switching to explicit engine, which supports CSGs...

Parsing properties file "dpgg_p3.props"...

4 properties:
(1) <<player1>>R{"utility1"}max=? [ F rounds=k ]
(2) <<player2>>R{"utility2"}max=? [ F rounds=k ]
(3) <<player3>>R{"utility3"}max=? [ F rounds=k ]
(4) <<player1:player2:player3>>max=? (R{"utility1"}[F s=1&rounds=k] + R{"utility2"}[F s=1&rounds=k] + R{"utility3"}[F s=1&rounds=k])

---------------------------------------------------------------------

Model checking: <<player1>>R{"utility1"}max=? [ F rounds=k ]
Model constants: f=1.6,k=2

Building model...
Model constants: f=1.6,k=2

Computing reachable states... 11056 states
Reachable states exploration and model construction done in 0.82 secs.
Sorting reachable states list...

Time for model construction: 0.893 seconds.

Warning: Deadlocks detected and fixed in 2277 states

Type:        CSG
States:      11056 (1 initial)
Transitions: 15151
Choices:     15151
Max/avg:     64/1.37
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.09,1.02)
Max/avg (actions): (4,16)/(1.02,1.09)
target=4554, inf=2277, rest=4225
Computing the upper bound where 0.28 is used instead of 0.0

Starting value iteration...

Value iteration converged after 5 iterations.
Computed an over-approximation of the solution (in 0.336 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 5 iterations.
Expected reachability took 1.63 seconds.
Precomputation took 1.011 seconds.

Value in the initial state: 0.0

Time for model checking: 1.659 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player2>>R{"utility2"}max=? [ F rounds=k ]
Model constants: f=1.6,k=2
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.09,1.02)
Max/avg (actions): (4,16)/(1.02,1.09)
target=4554, inf=2277, rest=4225
Computing the upper bound where 0.28 is used instead of 0.0

Starting value iteration...

Value iteration converged after 5 iterations.
Computed an over-approximation of the solution (in 0.164 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 5 iterations.
Expected reachability took 0.727 seconds.
Precomputation took 0.432 seconds.

Value in the initial state: 0.0

Time for model checking: 0.742 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player3>>R{"utility3"}max=? [ F rounds=k ]
Model constants: f=1.6,k=2
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.09,1.02)
Max/avg (actions): (4,16)/(1.02,1.09)
target=4554, inf=2277, rest=4225
Computing the upper bound where 0.28 is used instead of 0.0

Starting value iteration...

Value iteration converged after 5 iterations.
Computed an over-approximation of the solution (in 0.133 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 5 iterations.
Expected reachability took 0.463 seconds.
Precomputation took 0.238 seconds.

Value in the initial state: 0.0

Time for model checking: 0.476 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player1:player2:player3>>max=? (R{"utility1"}[F s=1&rounds=k] + R{"utility2"}[F s=1&rounds=k] + R{"utility3"}[F s=1&rounds=k])
Model constants: f=1.6,k=2

Building extended model...

States:      11056 (1 initial)
Transitions: 15151
Choices:     15151
Max/avg:     64/1.37

Max/avg (actions): (4,4,4)/(1.02,1.02,1.02)

Warning: Cannot use labelled polytoples on multi-coalitional properties. Changing to support enumeration.
0: (0.0,0.0,0.0)
1: (0.0,0.0,0.0)
0--1--2--3--4--5--6--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4--5--6--7--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4--5--6--7--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--2: (0.0,0.0,0.0)
0--1--2--3--4--5--6--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4--5--6--7--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4--5--6--7--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--3: (0.0,0.0,0.0)
0--1--2--3--4--5--6--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4--5--6--7--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4--5--6--7--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4: (9.0,9.0,9.0)
0--1--2--3--4--5--6--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4--5--6--7--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--4--5--6--7--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--0--1--2--3--5: (9.0,9.0,9.0)

Result for coalition player1: 9.0 (value in the intial state).
Result for coalition player2: 9.0 (value in the intial state).
Result for coalition player3: 9.0 (value in the intial state).

Value in the initial state: 27.0

Time for model checking: 4.153 seconds.

Result: 27.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 2 warnings during computation.

