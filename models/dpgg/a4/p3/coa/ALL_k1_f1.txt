PRISM-games
===========

Version: 3.0.beta (based on PRISM 4.5.dev)
Date: Fri Apr 03 10:13:51 CEST 2020
Hostname: Pierres-MBP.fritz.box
Memory limits: cudd=1g, java(heap)=1g
Command line: prism-games dpgg_p3_a4.prism dpgg_p3.props -const k=1 -const f=1

Parsing model file "dpgg_p3_a4.prism"...

Type:        CSG
Modules:     contributor1 contributor2 contributor3 rounds scheduler 
Variables:   c1 p1 c2 p2 c3 p3 rounds s 

Switching to explicit engine, which supports CSGs...

Parsing properties file "dpgg_p3.props"...

4 properties:
(1) <<player1>>R{"utility1"}max=? [ F rounds=k ]
(2) <<player2>>R{"utility2"}max=? [ F rounds=k ]
(3) <<player3>>R{"utility3"}max=? [ F rounds=k ]
(4) <<player1:player2:player3>>max=? (R{"utility1"}[F s=1&rounds=k] + R{"utility2"}[F s=1&rounds=k] + R{"utility3"}[F s=1&rounds=k])

---------------------------------------------------------------------

Model checking: <<player1>>R{"utility1"}max=? [ F rounds=k ]
Model constants: f=1,k=1

Building model...
Model constants: f=1,k=1

Computing reachable states... 176 states
Reachable states exploration and model construction done in 0.117 secs.
Sorting reachable states list...

Time for model construction: 0.141 seconds.

Warning: Deadlocks detected and fixed in 37 states

Type:        CSG
States:      176 (1 initial)
Transitions: 239
Choices:     239
Max/avg:     64/1.36
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.09,1.02)
Max/avg (actions): (4,16)/(1.02,1.09)
target=74, inf=37, rest=65
Computing the upper bound where 0.15 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.046 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.153 seconds.
Precomputation took 0.073 seconds.

Value in the initial state: 0.0

Time for model checking: 0.185 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player2>>R{"utility2"}max=? [ F rounds=k ]
Model constants: f=1,k=1
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.09,1.02)
Max/avg (actions): (4,16)/(1.02,1.09)
target=74, inf=37, rest=65
Computing the upper bound where 0.15 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.05 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.112 seconds.
Precomputation took 0.032 seconds.

Value in the initial state: 0.0

Time for model checking: 0.113 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player3>>R{"utility3"}max=? [ F rounds=k ]
Model constants: f=1,k=1
Building reward structure...

Starting expected reachability...
Max/avg (actions): (16,4)/(1.09,1.02)
Max/avg (actions): (4,16)/(1.02,1.09)
target=74, inf=37, rest=65
Computing the upper bound where 0.15 is used instead of 0.0

Starting value iteration...

Value iteration converged after 3 iterations.
Computed an over-approximation of the solution (in 0.004 seconds), this will now be used to get the solution

Starting value iteration...

Value iteration converged after 3 iterations.
Expected reachability took 0.037 seconds.
Precomputation took 0.013 seconds.

Value in the initial state: 0.0

Time for model checking: 0.038 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: <<player1:player2:player3>>max=? (R{"utility1"}[F s=1&rounds=k] + R{"utility2"}[F s=1&rounds=k] + R{"utility3"}[F s=1&rounds=k])
Model constants: f=1,k=1

Building extended model...

States:      176 (1 initial)
Transitions: 239
Choices:     239
Max/avg:     64/1.36

Max/avg (actions): (4,4,4)/(1.02,1.02,1.02)

Warning: Cannot use labelled polytoples on multi-coalitional properties. Changing to support enumeration.
0: (0.0,0.0,0.0)
1: (0.0,0.0,0.0)
2: (9.0,9.0,9.0)
3: (9.0,9.0,9.0)

Result for coalition player1: 9.0 (value in the intial state).
Result for coalition player2: 9.0 (value in the intial state).
Result for coalition player3: 9.0 (value in the intial state).

Value in the initial state: 27.0

Time for model checking: 0.446 seconds.

Result: 27.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 2 warnings during computation.

